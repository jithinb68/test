
var ctx = document.getElementById('myChart');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
    labels: [30+' Jan',31+' Jan',1+' Feb',2+' Feb',3+' Feb',4+' Feb',5+' Feb'],
    datasets: [{ 
        data: [20,37,50,50,62,70,84],
        borderColor: "#4ae4cf",
        fill: false,
        pointBorderWidth: 3,
        pointBackgroundColor: "white",
        pointRadius: 5,
        backgroundColor: [	
              'rgba(74, 228, 207, 0.1	)',
          ],
          fill: true,
      }, 
    ]
  },
    options:  {
         scales:
        {
          xAxes: [{
                gridLines : {
                    display : false,
                },
                ticks: {
                beginAtZero: true
            }
            }],
            yAxes: [{
               gridLines: {
        			drawBorder: false,
  				},
            ticks: {
                min: 0,
                max: 100,
                stepSize: 25
            }
            }]
        },
        legend: {
            display: false
         },
         tooltips: {
            enabled: false
         }
  }
});

var ctx = document.getElementById('myChart1');
var myChart1 = new Chart(ctx, {
    type: 'line',
    data: {
    labels: [0,1,2,3,4,5],
    datasets: [{ 
        data: [2,5,2,3,1,2],
        borderColor: "#b67eff", 
        fill: false,
        pointRadius: 0,
        backgroundColor: [	
              'rgba(182, 126, 255,0.1 )',
          ],
          fill: true,
      }, 
    ]
  },
    options:  {
         scales:
        {
          xAxes: [{
                gridLines : {
                    display : false,
                    drawBorder: false,
                },
                ticks: {
               display: false,
            }
            }],
            yAxes: [{
               gridLines: {
        			drawBorder: false,
        			display : false,
  				},

            ticks: {
                display: false,
            }
            }]
        },
        legend: {
            display: false
         },
         tooltips: {
            enabled: false
         }
  }
});

var ctx = document.getElementById('myChart2');
var myChart2 = new Chart(ctx, {
    type: 'line',
    data: {
    labels: [0,1,2,3,4,5],
    datasets: [{ 
        data: [4,3,5,2,3,1],
        borderColor: "#4ae4cf",
        fill: false,
        pointRadius: 0,
        backgroundColor: [	
              'rgba(74, 228, 207, 0.1	)',
          ],
          fill: true,
      }, 
    ]
  },
    options:  {
         scales:
        {
          xAxes: [{
                gridLines : {
                    display : false,
                    drawBorder: false,
                },
                ticks: {
               display: false,
            }
            }],
            yAxes: [{
               gridLines: {
        			drawBorder: false,
        			display : false,
  				},

            ticks: {
               display: false,
            }
            }]
        },
        legend: {
            display: false
         },
         tooltips: {
            enabled: false
         }
  }
});

var ctx = document.getElementById('myChart3');
var myChart3 = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25],
        datasets: [{
            data: [0,1,2,3,4,5,4,3,2,1,2,3,4,5,4,3,2,1,0],
            backgroundColor: 'rgba(51, 153, 51, 1)',   
            
        }]
    },
    options: {
    	legend: {
        display: false
      },
        scales: {
            yAxes: [{
            	gridLines: {
        			drawBorder: false,
        			display : false,
  				},
                ticks: {
                    display: false,
                }
            }],
            xAxes: [{
            	barThickness: 2,
            	gridLines: {
        			drawBorder: false,
        			display : false,
  				},
                ticks: {
                    display: false,
                }
                
            }],

        }
    }
});


var ctx = document.getElementById('myChart5');
var myChart5 = new Chart(ctx, {
    type: 'bar',
    data: {
         labels: [0,1,3,4,5,6,7,8,9,10,11,11],
         datasets: [{
         data: [0,0,-1,0,-11,-11,0,-11,-1,-1,0,0,-1,-11,0,-11],
         backgroundColor: 'rgb(229, 101, 73)'
      }, {
        
         data: [11,11,0,11,0,0,11,0,0,0,11,11,0,11,0],
         backgroundColor: 'rgb(99, 206, 242)'
      }]
    },
    options: {
    	legend: {
        display: false
      },
        scales: {
            yAxes: [{
            	gridLines: {
            		stacked : true,
        			drawBorder: false,
        			display : false,
  				},
                ticks: {
                    display: false,
                }
            }],
            xAxes: [{
            	stacked: true,
            	barThickness: 6,
            	gridLines: {
        			drawBorder: false,
        			display : false,
  				},
                ticks: {
                    display: false,
                }
                
            }],

        }
    }
});

var ctx = document.getElementById('myChart4');
var myChart4 = new Chart(ctx, {
    type: 'bar',
    data: {
         labels: [11,1,3,4,5,6,7,3,9,3,11,1],
         datasets: [{
         data: [1,3,1,1,1,0,1,1,1,2,1,1,1],
         backgroundColor: 'rgb(98, 241, 227)'
      }, {
        
         data: [3,4,4,3,4,2,3,3,3,5,3,3,4],
         backgroundColor: 'rgb(229, 101, 73)'
      }]
    },
    options: {
    	legend: {
        display: false
      },
        scales: {
            yAxes: [{
            	gridLines: {
            		stacked : true,
        			drawBorder: false,
        			display : false,
  				},
                ticks: {
                    display: false,
                }
            }],
            xAxes: [{
            	stacked: true,
            	barThickness: 6,
            	gridLines: {
        			drawBorder: false,
        			display : false,
  				},
                ticks: {
                    display: false,
                }
                
            }],

        }
    }
});

var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
  dropdown[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var dropdownContent = this.nextElementSibling;
    if (dropdownContent.style.display === "block") {
      dropdownContent.style.display = "none";
    } else {
      dropdownContent.style.display = "block";
    }
  });
}

// function togglesidebar(){
// 	var x=document.getElementById("mysidebar");
// 	var y=document.getElementById("headcont");
// 	if (x.style.display === "none") {
//     x.style.display = "block";
//   } else {
//     x.style.display = "none";
//     y.style.width = "100%";

//   }
// }


$( "#toggle-icon" ).click(function() {
  $("#mysidebar").hide();
  $(this).hide();
  $("#toggle-icon-open").show();
  $("#toggle-icon-open").css("display", "inline-block");
  $(".header-content").addClass("width100");
});

$( "#toggle-icon-open" ).click(function() {
  $("#mysidebar").show();
  $(this).hide();
  $(".open-menu").show();
  $(".header-content").removeClass("width100");
});



